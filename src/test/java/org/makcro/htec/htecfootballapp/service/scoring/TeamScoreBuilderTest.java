package org.makcro.htec.htecfootballapp.service.scoring;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Team;

import java.util.HashMap;

public class TeamScoreBuilderTest {

    private TeamScoreBuilderImpl teamScoreBuilder;

    @Before
    public void setUp() throws Exception {
        teamScoreBuilder = new TeamScoreBuilderImpl(new HashMap<>(), null);
    }

    @Test
    public void testRegisterTeamScore_whenTeamNotRegistered_ok() {
        Team team = getHomeTeam();
        TeamScoreResource teamScoreResource = teamScoreBuilder.registerTeamScore(team);

        Assert.assertEquals(team.getTitle(), teamScoreResource.getTeamName());
        Assert.assertEquals(teamScoreResource, teamScoreBuilder.getTeamScore(team));
    }

    @Test
    public void testRegisterTeamScore_whenTeamAlreadyRegisteredRegistered_ok() {
        Team team = getHomeTeam();
        HashMap<Team, TeamScoreResource> map = new HashMap<>();
        TeamScoreResource teamScoreResource = new TeamScoreResource();
        map.put(team, teamScoreResource);

        this.teamScoreBuilder = new TeamScoreBuilderImpl(map, null);
        TeamScoreResource result = teamScoreBuilder.registerTeamScore(team);

        Assert.assertEquals(teamScoreResource, result);
    }

    @Test
    public void testAddTeamsToMap_ok() {
        FootballMatch footballMatch = getFootballMatch();

        teamScoreBuilder = new TeamScoreBuilderImpl(new HashMap<>(), footballMatch);

        teamScoreBuilder.addTeamsToMap();
        Assert.assertNotNull(teamScoreBuilder.getHomeTeamScoreResource());
        Assert.assertNotNull(teamScoreBuilder.getAwayTeamScoreResource());
    }

    private Team getAwayTeam() {
        Team team = new Team();
        team.setTitle("Away team");
        return team;
    }

    private Team getHomeTeam() {
        Team team = new Team();
        team.setTitle("Home team");
        return team;
    }

    private FootballMatch getFootballMatch() {
        FootballMatch footballMatch = new FootballMatch();
        footballMatch.setHomeTeam(getHomeTeam());
        footballMatch.setAwayTeam(getAwayTeam());
        footballMatch.setScore("3:2");
        return footballMatch;
    }

    @Test
    public void testUpdateTeamScores() throws Exception {

        TeamScoreResource homeTeamScoreResource = new TeamScoreResource();
        TeamScoreResource awayTeamScoreResource = new TeamScoreResource();

        FootballMatch match = getFootballMatch();
        teamScoreBuilder = new TeamScoreBuilderImpl(new HashMap<>(), match);
        teamScoreBuilder.setHomeTeamScoreResource(homeTeamScoreResource);
        teamScoreBuilder.setAwayTeamScoreResource(awayTeamScoreResource);
        teamScoreBuilder.setScore(new int[]{3, 2});

        teamScoreBuilder.updateTeamScores();


        Assert.assertEquals(3, homeTeamScoreResource.getGoals());
        Assert.assertEquals(2, homeTeamScoreResource.getGoalsAgainst());
        Assert.assertEquals(2, awayTeamScoreResource.getGoals());
        Assert.assertEquals(3, awayTeamScoreResource.getGoalsAgainst());
    }

    @Test
    public void testUpdateWinLoseDraw_when_homeTeamWins() throws Exception {

        TeamScoreResource homeTeamScoreResource = new TeamScoreResource();
        TeamScoreResource awayTeamScoreResource = new TeamScoreResource();

        teamScoreBuilder = new TeamScoreBuilderImpl(new HashMap<>(), getFootballMatch());
        teamScoreBuilder.setMatchResult(MatchResult.HOME_TEAM_VICTORY);
        teamScoreBuilder.setHomeTeamScoreResource(homeTeamScoreResource);
        teamScoreBuilder.setAwayTeamScoreResource(awayTeamScoreResource);

        teamScoreBuilder.updateWinLoseDraw();




        Assert.assertEquals(1, homeTeamScoreResource.getWin());
        Assert.assertEquals(1, awayTeamScoreResource.getLose());
        Assert.assertEquals(0, homeTeamScoreResource.getDraw());
        Assert.assertEquals(0, awayTeamScoreResource.getDraw());
    }

    @Test
    public void testUpdateWinLoseDraw_when_awayTeamWins() throws Exception {
        TeamScoreResource homeTeamScoreResource = new TeamScoreResource();
        TeamScoreResource awayTeamScoreResource = new TeamScoreResource();

        teamScoreBuilder = new TeamScoreBuilderImpl(new HashMap<>(), getFootballMatch());
        teamScoreBuilder.setMatchResult(MatchResult.AWAY_TEAM_VICTORY);
        teamScoreBuilder.setHomeTeamScoreResource(homeTeamScoreResource);
        teamScoreBuilder.setAwayTeamScoreResource(awayTeamScoreResource);

        teamScoreBuilder.updateWinLoseDraw();

        Assert.assertEquals(1, homeTeamScoreResource.getLose());
        Assert.assertEquals(1, awayTeamScoreResource.getWin());
        Assert.assertEquals(0, homeTeamScoreResource.getDraw());
        Assert.assertEquals(0, awayTeamScoreResource.getDraw());
    }

    @Test
    public void testUpdateWinLoseDraw_when_draw() throws Exception {
        TeamScoreResource homeTeamScoreResource = new TeamScoreResource();
        TeamScoreResource awayTeamScoreResource = new TeamScoreResource();

        teamScoreBuilder = new TeamScoreBuilderImpl(new HashMap<>(), getFootballMatch());
        teamScoreBuilder.setMatchResult(MatchResult.DRAW);
        teamScoreBuilder.setHomeTeamScoreResource(homeTeamScoreResource);
        teamScoreBuilder.setAwayTeamScoreResource(awayTeamScoreResource);

        teamScoreBuilder.updateWinLoseDraw();

        Assert.assertEquals(1, homeTeamScoreResource.getDraw());
        Assert.assertEquals(1, awayTeamScoreResource.getDraw());

        Assert.assertEquals(0, homeTeamScoreResource.getLose());
        Assert.assertEquals(0, homeTeamScoreResource.getWin());
        Assert.assertEquals(0, awayTeamScoreResource.getLose());
        Assert.assertEquals(0, awayTeamScoreResource.getWin());
    }

    @Test
    public void testTeamScoreResource() {
        TeamScoreResource teamScoreResource = new TeamScoreResource();
        teamScoreResource.setDraw(1);
        teamScoreResource.setLose(1);
        teamScoreResource.setWin(1);
        teamScoreResource.setGoals(5);
        teamScoreResource.setGoalsAgainst(3);

        Assert.assertEquals(3, teamScoreResource.getPlayedGames());
        Assert.assertEquals(4, teamScoreResource.getPoints());
        Assert.assertEquals(5, teamScoreResource.getGoals());
        Assert.assertEquals(2, teamScoreResource.getGoalDifference());

    }
}
