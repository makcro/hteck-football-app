package org.makcro.htec.htecfootballapp.service.scoring;

import org.junit.Assert;
import org.junit.Test;

public class ScoringUtilsTest {

    @Test
    public void testParseScores_ok() throws Exception {
        String score = "1:3";
        int[] result = ScoringUtils.parseScoreString(score);
        Assert.assertEquals(1, result[0]);
        Assert.assertEquals(3, result[1]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseScore_whenNull_throwException() throws Exception {
        int[] result = ScoringUtils.parseScoreString(null);
    }

    @Test
    public void testParseMatchResult_homeTeamWin() throws Exception {
        int[] scores = new int[]{1, 0};
        MatchResult result = ScoringUtils.resolveMatchResult(scores);
        Assert.assertEquals(MatchResult.HOME_TEAM_VICTORY, result);
    }

    @Test
    public void testParseMatchResult_awayTeamWin() throws Exception {
        int[] scores = new int[]{0, 1};
        MatchResult result = ScoringUtils.resolveMatchResult(scores);
        Assert.assertEquals(MatchResult.AWAY_TEAM_VICTORY, result);
    }

    @Test
    public void testParseMatchResult_draw() throws Exception {
        int[] scores = new int[]{0, 0};
        MatchResult result = ScoringUtils.resolveMatchResult(scores);
        Assert.assertEquals(MatchResult.DRAW, result);
    }
}
