package org.makcro.htec.htecfootballapp.api.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.Payload;
import java.lang.annotation.Annotation;

public class ScoreValidatorTest {

    private ScoreValidator scoreValidator;

    @Before
    public void setUp() throws Exception {
        scoreValidator = new ScoreValidator();
        scoreValidator.initialize(new Score(){
            @Override
            public Class<? extends Annotation> annotationType() {
                return null;
            }

            @Override
            public String message() {
                return null;
            }

            @Override
            public Class<?>[] groups() {
                return new Class[0];
            }

            @Override
            public Class<? extends Payload>[] payload() {
                return new Class[0];
            }

            @Override
            public String format() {
                return "(^\\d{1,3}:\\d{1,3})";
            }
        });
    }

    @Test
    public void testIsValid_when_noDigits_then_false() {
        String invalidScore = "a:m";
        boolean result = this.scoreValidator.isValid(invalidScore, null);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsValid_when_tooLong_then_false() {
        String invalidScore = "1234:1233";
        boolean result = this.scoreValidator.isValid(invalidScore, null);
        Assert.assertFalse(result);
    }

    @Test
    public void testIsValid_when_1digit_then_true() {
        String invalidScore = "1:1";
        boolean result = this.scoreValidator.isValid(invalidScore, null);
        Assert.assertTrue(result);
    }
}
