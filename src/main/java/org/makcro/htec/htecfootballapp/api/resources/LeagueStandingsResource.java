package org.makcro.htec.htecfootballapp.api.resources;

import java.util.LinkedList;
import java.util.List;

public class LeagueStandingsResource {
    private String leagueTitle;
    private List<GroupStandingsResource> groups = new LinkedList<>();

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public List<GroupStandingsResource> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupStandingsResource> groups) {
        this.groups = groups;
    }
}
