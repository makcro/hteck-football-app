package org.makcro.htec.htecfootballapp.api.resources;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.chrono.JapaneseChronology;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MatchResourceDesiralizer extends StdDeserializer<MatchResourceWrapper> {

    public MatchResourceDesiralizer() {
        super(MatchResourceWrapper.class);
    }

    @Override
    public MatchResourceWrapper deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        MatchResultResource[] matchResultResources = jsonParser.getCodec().readValue(jsonParser, MatchResultResource[].class);

        MatchResourceWrapper matchResourceWrapper = new MatchResourceWrapper();
        matchResourceWrapper.setMatchResultResources(Arrays.asList(matchResultResources));
        return matchResourceWrapper;
    }
}
