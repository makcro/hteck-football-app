package org.makcro.htec.htecfootballapp.api.validation;

public class ValidationError {
    public String field;
    public String error;

    public ValidationError(String field, String error) {
        this.field = field;
        this.error = error;
    }

    public ValidationError() {
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
