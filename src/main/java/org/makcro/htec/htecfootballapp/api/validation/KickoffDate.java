package org.makcro.htec.htecfootballapp.api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ScoreValidator.class})
public @interface KickoffDate {
    String message() default "Invalid kickoff time format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
