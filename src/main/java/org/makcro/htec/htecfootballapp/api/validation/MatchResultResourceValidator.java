package org.makcro.htec.htecfootballapp.api.validation;

import org.makcro.htec.htecfootballapp.api.resources.MatchResultResource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class MatchResultResourceValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return MatchResultResource.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MatchResultResource resource = (MatchResultResource) o;
        ValidationUtils.rejectIfEmpty(errors, "leagueTitle", "League title may not be empty");

    }
}
