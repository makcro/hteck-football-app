package org.makcro.htec.htecfootballapp.api;

import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;

import java.util.Date;

public class SearchParam {
    public String groupName;
    public String team;
    private Group group;
    private Integer matchDay;
    private League league;
    private Date kickoff;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    public void setMatchDay(Integer matchDay) {
        this.matchDay = matchDay;
    }

    public Integer getMatchDay() {
        return matchDay;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public League getLeague() {
        return league;
    }

    public Date getKickoff() {
        return kickoff;
    }

    public void setKickoff(Date kickoff) {
        this.kickoff = kickoff;
    }
}
