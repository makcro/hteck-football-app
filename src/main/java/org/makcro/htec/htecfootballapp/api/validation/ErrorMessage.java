package org.makcro.htec.htecfootballapp.api.validation;

public class ErrorMessage {
    private String errorMessage;

    public ErrorMessage(String message) {

        errorMessage = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
