package org.makcro.htec.htecfootballapp.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class KickoffTimeValidator implements ConstraintValidator<KickoffDate, String> {

    private String dateFormat = "^((19|20)\\d{2})-((0|1)?\\d{1})-((0|1|2|3)?\\d{1})'T'((0|1|2)\\d{1}):((0|1|2|3|4|5)\\d{1}):((0|1|2|3|4|5)\\d{1})";

    @Override
    public void initialize(KickoffDate kickoffDate) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return false;
    }
}
