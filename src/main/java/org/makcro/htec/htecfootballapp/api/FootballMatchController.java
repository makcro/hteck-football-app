package org.makcro.htec.htecfootballapp.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.makcro.htec.htecfootballapp.api.resources.LeagueStandingsResource;
import org.makcro.htec.htecfootballapp.api.resources.MatchResourceWrapper;
import org.makcro.htec.htecfootballapp.api.resources.MatchResultResource;
import org.makcro.htec.htecfootballapp.api.validation.ValidationError;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.makcro.htec.htecfootballapp.service.FootballMatchService;
import org.makcro.htec.htecfootballapp.api.resources.GroupStandingsResource;
import org.makcro.htec.htecfootballapp.service.GroupService;
import org.makcro.htec.htecfootballapp.service.LeagueService;
import org.makcro.htec.htecfootballapp.service.scoring.ScoringService;
import org.makcro.htec.htecfootballapp.service.scoring.ScoringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import sun.nio.cs.FastCharsetProvider;

import javax.validation.Valid;
import java.io.IOException;
import java.rmi.MarshalException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class FootballMatchController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private FootballMatchService footballMatchService;

    @Autowired
    private ScoringService scoringService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
    }

    @RequestMapping(method = RequestMethod.POST, value = ApiUrls.GROUPS_RESULTS_URL, consumes = {
            MediaType.APPLICATION_JSON_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupStandingsResource> processGroupResults(@RequestBody @Valid MatchResourceWrapper matchResultResourceWrapper) {
        if (matchResultResourceWrapper == null || matchResultResourceWrapper.getMatchResultResources().isEmpty()) {
            throw new IllegalArgumentException("Match results not supplied");
        }
        GroupStandingsResource groupStandingsResource = footballMatchService.processGroupResults(matchResultResourceWrapper.getMatchResultResources());
        return new ResponseEntity<>(groupStandingsResource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = ApiUrls.LEAGUE_RESULTS_URL, consumes = {
            MediaType.APPLICATION_JSON_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LeagueStandingsResource> processLeagueResults(@RequestBody @Valid MatchResourceWrapper matchResultResourceWrapper) {
        if (matchResultResourceWrapper == null || matchResultResourceWrapper.getMatchResultResources().isEmpty()) {
            throw new IllegalArgumentException("Match results not supplied");
        }

        LeagueStandingsResource leagueStandingsResource = footballMatchService.processLeagueResults(matchResultResourceWrapper.getMatchResultResources());
        return new ResponseEntity<>(leagueStandingsResource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = { ApiUrls.MATCH_RESULT_BASE_URL }, consumes = {
            MediaType.APPLICATION_JSON_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupStandingsResource> processSingleResult(@RequestBody @Valid MatchResultResource matchResult) {
        GroupStandingsResource groupStandingsResource = footballMatchService.processSingleMatchResult(matchResult);
        return new ResponseEntity<>(groupStandingsResource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = ApiUrls.LEAGUE_STANDINGS_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LeagueStandingsResource> getLeagueStandings(@PathVariable(name = "leagueId", required = true) Long leagueIdParam,
            @RequestParam(name = "matchDay", required = false) Integer matchDay) {

        Optional<LeagueStandingsResource> leagueStandingsResource = scoringService.generateLeagueScores(leagueIdParam, matchDay);
        if (leagueStandingsResource.isPresent()) {
            return new ResponseEntity<>(leagueStandingsResource.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.GET, value = ApiUrls.GROUPS_STANDINGS_URL, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<GroupStandingsResource> getGropuStandings(@PathVariable(name = "groupId") String groupId,
            @RequestParam(required = false, name = "matchDay") Integer matchDay) {
        Optional<GroupStandingsResource> optionalGroupStandings = scoringService.generateGroupScores(groupId, matchDay);
        if (optionalGroupStandings.isPresent()) {
            return new ResponseEntity<>(optionalGroupStandings.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.PUT, value = ApiUrls.MATCH_RESULT_INDIVIDUAL_URL, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupStandingsResource> updateMatch(@PathVariable(name = "matchId") Long matchId,
            @RequestBody @Valid MatchResultResource matchResultResource) {

        GroupStandingsResource groupStandingsResource = footballMatchService.updateMatch(matchId, matchResultResource);
        return new ResponseEntity<>(groupStandingsResource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/matches", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MatchResultResource>> searchMatches(@RequestParam(name = "search", required = false) String searchDto) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(ScoringUtils.dateFormater);
        try {
            SearchParam searchParam = searchDto != null ? objectMapper.readValue(searchDto, SearchParam.class) : new SearchParam();
            List<MatchResultResource> matchResultResources = footballMatchService.search(searchParam);
            if (matchResultResources.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(matchResultResources, HttpStatus.OK);
        } catch (IOException e) {
            throw new IllegalArgumentException("Invalid search parameters");
        }

    }

    @RequestMapping(method = RequestMethod.GET, value = ApiUrls.LEAGUE_INDIVIDUAL_URL + "/matches", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MatchResultResource>> getLeagueMatches(@PathVariable(name = "leagueId", required = true) Long leagueIdParam) {
        League league = leagueService.findById(leagueIdParam);
        if (league == null) {
            throw new IllegalArgumentException("Invalid league id");
        }

        SearchParam searchParam = new SearchParam();
        searchParam.setLeague(league);
        List<MatchResultResource> matchResultResources = footballMatchService.search(searchParam);
        if (matchResultResources.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(matchResultResources, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = ApiUrls.GROUPS_INDIVIDUAL_URL + "/matches", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MatchResultResource>> getGroupMatches(@PathVariable(name = "leagueId", required = true) Long leagueIdParam,
            @PathVariable(name = "groupId", required = true) String groupName) {
        League league = leagueService.findById(leagueIdParam);

        if (league == null) {
            throw new IllegalArgumentException("Invalid league id");
        }

        Group group = groupService.findByTitleAndLeague(groupName, league);
        if (group == null) {
            throw new IllegalArgumentException("Invalid group name");
        }

        SearchParam searchParam = new SearchParam();
        searchParam.setLeague(league);
        searchParam.setGroup(group);

        List<MatchResultResource> matchResultResources = footballMatchService.search(searchParam);
        if (matchResultResources.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(matchResultResources, HttpStatus.OK);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ValidationError>> processValidationErrors(MethodArgumentNotValidException ex) {
        List<ValidationError> validationErrors = ex.getBindingResult().getFieldErrors().stream()
                .map(e -> new ValidationError(e.getField(), e.getDefaultMessage())).collect(Collectors.toList());

        return new ResponseEntity<>(validationErrors, HttpStatus.BAD_REQUEST);
    }
}
