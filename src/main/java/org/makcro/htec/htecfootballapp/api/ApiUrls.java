package org.makcro.htec.htecfootballapp.api;

public class ApiUrls {
    public static final String STANDINGS = "/standings";
    public static final String RESULTS = "/results";
    public static final String LEAGUE_BASE_URL = "/leagues";
    public static final String LEAGUE_RESULTS_URL = LEAGUE_BASE_URL + RESULTS;
    public static final String LEAGUE_INDIVIDUAL_URL = LEAGUE_BASE_URL + "/{leagueId}";
    public static final String GROUPS_BASE_URL = LEAGUE_INDIVIDUAL_URL + "/groups";
    public static final String GROUPS_RESULTS_URL = "/groups" + RESULTS;
    public static final String GROUPS_INDIVIDUAL_URL = GROUPS_BASE_URL + "/{groupId}";
    public static final String GROUPS_STANDINGS_URL = GROUPS_INDIVIDUAL_URL + STANDINGS;
    public static final String LEAGUE_STANDINGS_URL = LEAGUE_INDIVIDUAL_URL + STANDINGS;
    public static final String MATCH_RESULT_BASE_URL = "/matches/";
    public static final String MATCH_RESULT_INDIVIDUAL_URL = MATCH_RESULT_BASE_URL + "/{matchId}";
}
