package org.makcro.htec.htecfootballapp.api.resources;

import org.makcro.htec.htecfootballapp.service.scoring.TeamScoreResource;

import java.util.List;

public class GroupStandingsResource {
    private String leagueTitle;
    private String matchDay;
    private String group;
    private List<TeamScoreResource> standings;

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public String getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(String matchDay) {
        this.matchDay = matchDay;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<TeamScoreResource> getStandings() {
        return standings;
    }

    public void setStandings(List<TeamScoreResource> standings) {
        this.standings = standings;
    }
}
