package org.makcro.htec.htecfootballapp.api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ScoreValidator.class})
public @interface Score {
    String message() default "Invalid score format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String format() default "(^\\d{1,3}:\\d{1,3})";
}
