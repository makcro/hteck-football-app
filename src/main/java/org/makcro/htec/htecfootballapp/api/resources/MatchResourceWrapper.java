package org.makcro.htec.htecfootballapp.api.resources;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.validation.Valid;
import java.util.List;

@JsonDeserialize(using = MatchResourceDesiralizer.class)
public class MatchResourceWrapper {

    @Valid
    private List<MatchResultResource> matchResultResources;

    public List<MatchResultResource> getMatchResultResources() {
        return matchResultResources;
    }

    public void setMatchResultResources(List<MatchResultResource> matchResultResources) {
        this.matchResultResources = matchResultResources;
    }
}
