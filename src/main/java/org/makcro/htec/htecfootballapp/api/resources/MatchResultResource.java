package org.makcro.htec.htecfootballapp.api.resources;

import org.makcro.htec.htecfootballapp.api.validation.Score;

import javax.validation.constraints.NotNull;

public class MatchResultResource {
    private Long id;
    @NotNull(message = "League name is mandatory")
    private String leagueTitle;
    @NotNull
    private Integer matchday;
    @NotNull
    private String group;
    @NotNull
    private String homeTeam;
    @NotNull
    private String awayTeam;
    @NotNull
    private String kickoffAt;
    @NotNull
    @Score
    private String score;

    public MatchResultResource() {
    }

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public Integer getMatchday() {
        return matchday;
    }

    public void setMatchday(Integer matchday) {
        this.matchday = matchday;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getKickoffAt() {
        return kickoffAt;
    }

    public void setKickoffAt(String kickoffAt) {
        this.kickoffAt = kickoffAt;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
