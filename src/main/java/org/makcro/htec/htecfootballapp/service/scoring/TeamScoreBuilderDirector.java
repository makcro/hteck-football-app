package org.makcro.htec.htecfootballapp.service.scoring;

public class TeamScoreBuilderDirector {
    private TeamScoreBuilder teamScoreBuilder;

    public TeamScoreBuilderDirector(TeamScoreBuilder teamScoreBuilder) {
        this.teamScoreBuilder = teamScoreBuilder;
    }

    public void build() {
        this.teamScoreBuilder.addTeamsToMap();
        this.teamScoreBuilder.parseScore();
        this.teamScoreBuilder.resolveMatchResult();
        this.teamScoreBuilder.updateTeamScores();
        this.teamScoreBuilder.updateWinLoseDraw();
    }
}
