package org.makcro.htec.htecfootballapp.service.match_processor;

import org.makcro.htec.htecfootballapp.api.resources.MatchResultResource;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.makcro.htec.htecfootballapp.service.GroupService;
import org.makcro.htec.htecfootballapp.service.LeagueService;
import org.makcro.htec.htecfootballapp.service.TeamService;
import org.makcro.htec.htecfootballapp.service.scoring.ScoringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class EntityVerificationProcessor implements MatchProcessor {

    private LeagueService leagueService;
    private GroupService groupService;
    private TeamService teamService;

    @Autowired
    public EntityVerificationProcessor(LeagueService leagueService, GroupService groupService, TeamService teamService,
            @Value("${match.date.format}") String dateFormat) {
        this.leagueService = leagueService;
        this.groupService = groupService;
        this.teamService = teamService;
    }

    @Override
    public boolean process(MatchProcessorContext context) {
        try {
            verify(context);
            return true;
        } catch (ParseException e) {
            throw new IllegalArgumentException("Illegal date format");
        }
    }

    private void verify(MatchProcessorContext context) throws ParseException {
        MatchResultResource matchResultResource = context.getMatchResultResource();
        League league = leagueService.findByTitleOrCreate(matchResultResource.getLeagueTitle());
        Group group = groupService.findByTitleOrCreate(matchResultResource.getGroup(), league);
        Team homeTeam = teamService.findByTitleOrCreate(matchResultResource.getHomeTeam(), group);
        Team awayTeam = teamService.findByTitleOrCreate(matchResultResource.getAwayTeam(), group);

        context.setLeague(league);
        context.setGroup(group);
        context.setHomeTeam(homeTeam);
        context.setAwayTeam(awayTeam);
        context.setKickoffAt(ScoringUtils.parseKickoffDate(context.getMatchResultResource().getKickoffAt()));

        context.getProcessingResult().setLeague(league);
        context.getProcessingResult().addGroup(group);
    }
}
