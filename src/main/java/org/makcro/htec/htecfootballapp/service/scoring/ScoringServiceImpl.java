package org.makcro.htec.htecfootballapp.service.scoring;

import org.makcro.htec.htecfootballapp.api.SearchParam;
import org.makcro.htec.htecfootballapp.api.resources.GroupStandingsResource;
import org.makcro.htec.htecfootballapp.api.resources.LeagueStandingsResource;
import org.makcro.htec.htecfootballapp.data.*;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service("simpleScoringService")
public class ScoringServiceImpl implements ScoringService {

    @Autowired
    private FootballMatchRepository footballMatchRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private LeagueRepository leagueRepository;

    @Autowired
    private SearchableFootballMatchRepository searchableFootballMatchRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Optional<GroupStandingsResource> generateGroupScores(Group group, Integer matchDay) {
        SearchParam searchParam = new SearchParam();
        searchParam.setGroup(group);
        searchParam.setMatchDay(matchDay);

        List<FootballMatch> matchesByGroup = searchableFootballMatchRepository.search(searchParam);
        if (matchesByGroup.isEmpty()) {
            return Optional.empty();
        }

        GroupStandingsResource groupStandingsResource = generateGroupStandingsResource(group, matchesByGroup);
        return Optional.of(groupStandingsResource);
    }

    private GroupStandingsResource generateGroupStandingsResource(Group group, List<FootballMatch> matchesByGroup) {
        Map<Team, TeamScoreResource> teamMap = new HashMap<>();

        matchesByGroup.stream().forEach(m -> {
            TeamScoreBuilder teamScoreBuilder = new TeamScoreBuilderImpl(teamMap, m);
            TeamScoreBuilderDirector teamScoreBuilderDirector = new TeamScoreBuilderDirector(teamScoreBuilder);
            teamScoreBuilderDirector.build();
        });

        List<TeamScoreResource> teamScoreResources = teamMap.values().stream().collect(Collectors.toList());

        teamScoreResources.sort(new TeamScoreComparator());
        ScoringUtils.generateRanks(teamScoreResources);

        int matchDay = ScoringUtils.resolveMatchDay(matchesByGroup);

        GroupStandingsResource groupStandingsResource = new GroupStandingsResource();
        groupStandingsResource.setLeagueTitle(group.getLeague().getTitle());
        groupStandingsResource.setGroup(group.getTitle());
        groupStandingsResource.setStandings(teamScoreResources);
        groupStandingsResource.setMatchDay(String.valueOf(matchDay));
        return groupStandingsResource;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Optional<GroupStandingsResource> generateGroupScores(String groupName, Integer matchDay) {
        Group group = groupRepository.findByTitle(groupName);
        if (group == null) {
            throw new IllegalArgumentException("Group not found");
        }

        return generateGroupScores(group, matchDay);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<LeagueStandingsResource> generateLeagueScores(League league, Integer matchDay) {
        SearchParam searchParam = new SearchParam();
        searchParam.setLeague(league);
        searchParam.setMatchDay(matchDay);

        List<FootballMatch> footballMatches = searchableFootballMatchRepository.search(searchParam);

        if (footballMatches.isEmpty()) {
            return Optional.empty();
        }

        LeagueStandingsResource leagueStandingsResource = new LeagueStandingsResource();
        leagueStandingsResource.setLeagueTitle(league.getTitle());

        Map<Group, List<FootballMatch>> matchesByGroup = footballMatches.stream().collect(Collectors.groupingBy(fm -> fm.getGroup()));
        matchesByGroup.keySet().stream().forEach(g -> {
            GroupStandingsResource groupStandingsResource = generateGroupStandingsResource(g, matchesByGroup.get(g));
            leagueStandingsResource.getGroups().add(groupStandingsResource);
        });

        return Optional.of(leagueStandingsResource);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Optional<LeagueStandingsResource> generateLeagueScores(Long leagueId, Integer matchDay) {
        League league = leagueRepository.findOne(leagueId);
        if (league == null) {
            throw new IllegalArgumentException("League not found");
        }
        return generateLeagueScores(league, matchDay);
    }
}
