package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;

/**
 * Created by marko.lukic on 12/10/2017.
 */
public interface GroupService {
    Group findByTitleOrCreate(String title, League league);

    Group findByTitleAndLeague(String groupName, League league);
}
