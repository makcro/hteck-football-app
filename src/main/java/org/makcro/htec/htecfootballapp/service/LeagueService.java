package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.data.entity.League;

/**
 * Created by marko.lukic on 12/10/2017.
 */
public interface LeagueService {
    League findByTitleOrCreate(String title);

    League findById(Long leagueIdParam);
}
