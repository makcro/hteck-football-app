package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.data.GroupRepository;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by marko.lukic on 12/10/2017.
 */
@Service
public class GroupServiceImpl implements GroupService {

    private GroupRepository groupRepository;

    @Autowired
    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public Group findByTitleOrCreate(String title, League league) {
        Group group = groupRepository.findByTitle(title);
        if (group == null) {
            group = new Group();
            group.setTitle(title);
            group.setLeague(league);
            group = groupRepository.save(group);
        }
        return group;
    }

    @Override
    public Group findByTitleAndLeague(String groupName, League league) {
        return groupRepository.findByTitleAndLeague(groupName, league);
    }
}
