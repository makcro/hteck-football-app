package org.makcro.htec.htecfootballapp.service.scoring;

import org.makcro.htec.htecfootballapp.api.resources.GroupStandingsResource;
import org.makcro.htec.htecfootballapp.api.resources.LeagueStandingsResource;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;

import java.util.Optional;

public interface ScoringService {
    Optional<GroupStandingsResource> generateGroupScores(Group group, Integer matchDay);

    Optional<GroupStandingsResource> generateGroupScores(String groupName, Integer matchDay);

    Optional<LeagueStandingsResource> generateLeagueScores(League league, Integer matchDay);

    Optional<LeagueStandingsResource> generateLeagueScores(Long leagueId, Integer matchDay);
}
