package org.makcro.htec.htecfootballapp.service.scoring;

import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Team;

import java.util.Map;

public class TeamScoreBuilderImpl implements TeamScoreBuilder {
    private Map<Team, TeamScoreResource> teamScoreMap;
    private FootballMatch footballMatch;
    private int[] score;
    private MatchResult matchResult;
    private TeamScoreResource homeTeamScoreResource;
    private TeamScoreResource awayTeamScoreResource;

    public TeamScoreBuilderImpl(Map<Team, TeamScoreResource> teamScoreMap, FootballMatch footballMatch) {
        this.teamScoreMap = teamScoreMap;
        this.footballMatch = footballMatch;
    }

    @Override
    public void addTeamsToMap() {
        this.homeTeamScoreResource = registerTeamScore(footballMatch.getHomeTeam());
        this.awayTeamScoreResource = registerTeamScore(footballMatch.getAwayTeam());
    }

    @Override
    public void parseScore() {
        this.score = ScoringUtils.parseScoreString(footballMatch.getScore());
    }

    @Override
    public void resolveMatchResult(){
        this.matchResult = ScoringUtils.resolveMatchResult(this.score);
    }

    @Override
    public void updateTeamScores() {
        // update goals
        homeTeamScoreResource.setGoals(homeTeamScoreResource.getGoals() + score[0]);
        awayTeamScoreResource.setGoals(awayTeamScoreResource.getGoals() + score[1]);

        // scores against
        homeTeamScoreResource.setGoalsAgainst(homeTeamScoreResource.getGoalsAgainst() + score[1]);
        awayTeamScoreResource.setGoalsAgainst(awayTeamScoreResource.getGoalsAgainst() + score[0]);
    }

    @Override
    public void updateWinLoseDraw() {
        switch (matchResult) {
        case HOME_TEAM_VICTORY:
            homeTeamScoreResource.setWin(homeTeamScoreResource.getWin() + 1);
            awayTeamScoreResource.setLose(awayTeamScoreResource.getLose() + 1);
            break;
        case AWAY_TEAM_VICTORY:
            awayTeamScoreResource.setWin(awayTeamScoreResource.getWin() + 1);
            homeTeamScoreResource.setLose(homeTeamScoreResource.getLose() + 1);
            break;
        case DRAW:
            homeTeamScoreResource.setDraw(homeTeamScoreResource.getDraw() + 1);
            awayTeamScoreResource.setDraw(awayTeamScoreResource.getDraw() + 1);
        }
    }


    protected TeamScoreResource registerTeamScore(Team team) {
        if (teamScoreMap.get(team) == null){
            TeamScoreResource teamScoreResource = new TeamScoreResource();
            teamScoreResource.setTeamName(team.getTitle());
            teamScoreMap.put(team, teamScoreResource);
            return teamScoreResource;
        }
        return teamScoreMap.get(team);
    }

    public TeamScoreResource getTeamScore(Team team) {
        return teamScoreMap.get(team);
    }

    protected TeamScoreResource getHomeTeamScoreResource() {
        return homeTeamScoreResource;
    }

    protected TeamScoreResource getAwayTeamScoreResource() {
        return awayTeamScoreResource;
    }

    protected void setScore(int[] score) {
        this.score = score;
    }

    protected MatchResult getMatchResult() {
        return matchResult;
    }

    protected void setMatchResult(MatchResult matchResult) {
        this.matchResult = matchResult;
    }

    protected void setAwayTeamScoreResource(TeamScoreResource awayTeamScoreResource) {
        this.awayTeamScoreResource = awayTeamScoreResource;
    }

    protected void setHomeTeamScoreResource(TeamScoreResource homeTeamScoreResource) {
        this.homeTeamScoreResource = homeTeamScoreResource;
    }
}
