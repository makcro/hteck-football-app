package org.makcro.htec.htecfootballapp.service.match_processor;

/**
 * Created by marko.lukic on 12/11/2017.
 */
public interface MatchProcessor {
    boolean process(MatchProcessorContext context);
}
