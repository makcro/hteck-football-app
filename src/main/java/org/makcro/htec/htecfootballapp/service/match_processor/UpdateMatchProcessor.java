package org.makcro.htec.htecfootballapp.service.match_processor;

import org.makcro.htec.htecfootballapp.data.FootballMatchRepository;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateMatchProcessor implements MatchProcessor {

    private FootballMatchRepository footballMatchRepository;

    @Autowired
    public UpdateMatchProcessor(FootballMatchRepository footballMatchRepository) {
        this.footballMatchRepository = footballMatchRepository;
    }

    @Override
    public boolean process(MatchProcessorContext context) {
        FootballMatch match = footballMatchRepository
                .findByHomeTeamAndAwayTeamAndKickoffAt(context.getHomeTeam(), context.getAwayTeam(), context.getKickoffAt());
        if (match != null) {
            match.setScore(context.getMatchResultResource().getScore());
            match.setMatchDay((context.getMatchResultResource().getMatchday()));
            footballMatchRepository.save(match);
            return false;
        }
        return true;
    }


}
