package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.Team;

/**
 * Created by marko.lukic on 12/10/2017.
 */
public interface TeamService {
    Team findByTitleOrCreate(String title, Group group);
}
