package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.data.TeamRepository;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by marko.lukic on 12/10/2017.
 */
@Service
public class TeamServiceImpl implements TeamService {
    private TeamRepository teamRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public Team findByTitleOrCreate(String title, Group group){
        Team team = teamRepository.findByTitle(title);
        if (team == null) {
            team = new Team();
            team.setTitle(title);
            team = teamRepository.save(team);
        }

        return team;
    }
}
