package org.makcro.htec.htecfootballapp.service.scoring;

public enum MatchResult {
    HOME_TEAM_VICTORY, AWAY_TEAM_VICTORY, DRAW
}
