package org.makcro.htec.htecfootballapp.service.scoring;

import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.springframework.util.Assert;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class ScoringUtils {

    public static DateFormat dateFormater;

    static {
        dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormater.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private ScoringUtils(){
    }

    public static int[] parseScoreString(String score) {
        Assert.notNull(score, "Score cannot be null");
        String[] scoreStrings = score.split(":");
        int[] scores = new int[scoreStrings.length];
        scores[0] = Integer.parseInt(scoreStrings[0]);
        scores[1] = Integer.parseInt(scoreStrings[1]);
        return scores;
    }

    public static MatchResult resolveMatchResult(int[] scores){
        if (scores[0] > scores[1]) {
            return MatchResult.HOME_TEAM_VICTORY;
        } else if (scores[0] < scores[1]) {
            return MatchResult.AWAY_TEAM_VICTORY;
        }

        return MatchResult.DRAW;
    }

    public static int resolveMatchDay(Collection<FootballMatch> matches) {
        int matchDay = matches.stream()
                .mapToInt(FootballMatch::getMatchDay)
                .max().orElse(0);

        return matchDay;
    }

    public static void generateRanks(List<TeamScoreResource> teamScoreResources) {
        for (int i = 0; i < teamScoreResources.size(); ++i) {
            teamScoreResources.get(i).setRank(i + 1);
        }
    }

    public static Date parseKickoffDate(String raw) {
        try {
            return dateFormater.parse(raw);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid kickoff date format");
        }
    }
}
