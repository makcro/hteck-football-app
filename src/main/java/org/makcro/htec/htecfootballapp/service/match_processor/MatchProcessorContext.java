package org.makcro.htec.htecfootballapp.service.match_processor;

import org.makcro.htec.htecfootballapp.api.resources.MatchResultResource;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.makcro.htec.htecfootballapp.service.MatchProcessingResult;

import java.util.Date;

/**
 * Created by marko.lukic on 12/11/2017.
 */
public class MatchProcessorContext {

    private League league;
    private Group group;
    private Team homeTeam;
    private Team awayTeam;
    private FootballMatch footballMatch;
    private MatchResultResource matchResultResource;
    private MatchProcessingResult processingResult;
    private Date kickoffAt;

    private MatchProcessorContext(MatchResultResource matchResultResource, MatchProcessingResult result) {
        this.matchResultResource = matchResultResource;
        processingResult = result;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public FootballMatch getFootballMatch() {
        return footballMatch;
    }

    public void setFootballMatch(FootballMatch footballMatch) {
        this.footballMatch = footballMatch;
    }

    public MatchResultResource getMatchResultResource() {
        return matchResultResource;
    }

    public void setMatchResultResource(MatchResultResource matchResultResource) {
        this.matchResultResource = matchResultResource;
    }

    public static MatchProcessorContext of(MatchResultResource match, MatchProcessingResult result) {
        return new MatchProcessorContext(match, result);
    }

    public MatchProcessingResult getProcessingResult() {
        return processingResult;
    }

    public void setProcessingResult(MatchProcessingResult processingResult) {
        this.processingResult = processingResult;
    }

    public void setKickoffAt(Date kickoffAt) {
        this.kickoffAt = kickoffAt;
    }

    public Date getKickoffAt() {
        return kickoffAt;
    }
}
