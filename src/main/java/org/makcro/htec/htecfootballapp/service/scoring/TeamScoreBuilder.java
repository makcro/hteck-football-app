package org.makcro.htec.htecfootballapp.service.scoring;

public interface TeamScoreBuilder {
    void addTeamsToMap();

    void parseScore();

    void resolveMatchResult();

    void updateTeamScores();

    void updateWinLoseDraw();
}
