package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.api.SearchParam;
import org.makcro.htec.htecfootballapp.api.resources.LeagueStandingsResource;
import org.makcro.htec.htecfootballapp.api.resources.MatchResultResource;
import org.makcro.htec.htecfootballapp.data.FootballMatchRepository;
import org.makcro.htec.htecfootballapp.data.SearchableFootballMatchRepository;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.makcro.htec.htecfootballapp.service.match_processor.MatchProcessor;
import org.makcro.htec.htecfootballapp.service.match_processor.MatchProcessorContext;
import org.makcro.htec.htecfootballapp.api.resources.GroupStandingsResource;
import org.makcro.htec.htecfootballapp.service.scoring.ScoringService;
import org.makcro.htec.htecfootballapp.service.scoring.ScoringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FootballMatchServiceImpl implements FootballMatchService {

    @Autowired
    @Qualifier("processors")
    private List<MatchProcessor> processors;

    @Autowired
    private ScoringService scoringService;

    @Autowired
    private FootballMatchRepository footballMatchRepository;

    @Autowired
    private SearchableFootballMatchRepository searchableFootballMatchRepository;

    public FootballMatchServiceImpl() {
    }

    protected MatchProcessingResult processResultsInternal(List<MatchResultResource> matchResults) {
        MatchProcessingResult result = new MatchProcessingResult();
        matchResults.stream().forEach(match -> {

            MatchProcessorContext matchProcessorContext = MatchProcessorContext.of(match, result);
            for (MatchProcessor processor : processors) {
                boolean processingShouldContinue = processor.process(matchProcessorContext);
                if (!processingShouldContinue){
                    break;
                }
            }

        });
        return result;
    }

    @Override
//    @Transactional
    public GroupStandingsResource processGroupResults(List<MatchResultResource> matchResults){
        MatchProcessingResult matchProcessingResult = processResultsInternal(matchResults);

        Group group = matchProcessingResult.getGroups().iterator().next();
        Optional<GroupStandingsResource> groupStandings = scoringService.generateGroupScores(group, null);
        if (groupStandings.isPresent()) {
            return groupStandings.get();
        }

        return null;
    }

    @Override
//    @Transactional
    public LeagueStandingsResource processLeagueResults(List<MatchResultResource> matchResultResources) {
        MatchProcessingResult matchProcessingResult = processResultsInternal(matchResultResources);

        League league = matchProcessingResult.getLeague();
        Optional<LeagueStandingsResource> leagueStandingsResource = scoringService.generateLeagueScores(league, null);

        return leagueStandingsResource.get();
    }

    @Override
//    @Transactional
    public GroupStandingsResource processSingleMatchResult(MatchResultResource matchResult) {
        List<MatchResultResource> matches = Stream.of(matchResult).collect(Collectors.toList());
        return processGroupResults(matches);
    }

    @Override
    public GroupStandingsResource updateMatch(Long matchId, MatchResultResource matchResultResource) {
        FootballMatch footballMatch = footballMatchRepository.findOne(matchId);
        if (footballMatch == null) {
            throw new IllegalArgumentException("Match not found");
        }

        footballMatch.setMatchDay(matchResultResource.getMatchday());
        footballMatch.setScore(matchResultResource.getScore());
        footballMatch.setKickoffAt(ScoringUtils.parseKickoffDate(matchResultResource.getKickoffAt()));

        footballMatchRepository.save(footballMatch);
        Optional<GroupStandingsResource> groupStandingsResource = scoringService.generateGroupScores(footballMatch.getGroup(), null);
        return groupStandingsResource.get();
    }

    @Override
    public List<MatchResultResource> search(SearchParam searchParam) {
        List<FootballMatch> footballMatches = searchableFootballMatchRepository.search(searchParam);
        List<MatchResultResource> matchResultResources = footballMatches.stream().map(m -> {
            MatchResultResource resource = new MatchResultResource();
            resource.setAwayTeam(m.getAwayTeam().getTitle());
            resource.setMatchday(m.getMatchDay());
            resource.setGroup(m.getGroup().getTitle());
            resource.setHomeTeam(m.getHomeTeam().getTitle());
            resource.setKickoffAt(m.getKickoffAt().toString());
            resource.setLeagueTitle(m.getGroup().getLeague().getTitle());
            resource.setScore(m.getScore());
            resource.setId(m.getId());
            return resource;
        }).collect(Collectors.toList());

        return matchResultResources;
    }

}
