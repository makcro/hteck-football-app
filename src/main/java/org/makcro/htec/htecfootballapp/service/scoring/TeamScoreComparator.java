package org.makcro.htec.htecfootballapp.service.scoring;

public class TeamScoreComparator implements java.util.Comparator<TeamScoreResource> {

    @Override
    public int compare(TeamScoreResource o1, TeamScoreResource o2) {
        int pointDiff = o2.getPoints() - o1.getPoints();
        if (pointDiff != 0){
            return pointDiff;
        }

        int totalGoalsDiff = o2.getGoals() - o1.getGoals();
        if (totalGoalsDiff != 0) {
            return totalGoalsDiff;
        }

        return o2.getGoalDifference() - o1.getGoalDifference();
    }

}
