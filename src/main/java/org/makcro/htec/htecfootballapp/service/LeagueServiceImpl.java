package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.data.LeagueRepository;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by marko.lukic on 12/10/2017.
 */
@Service
public class LeagueServiceImpl implements LeagueService {
    private LeagueRepository leagueRepository;

    @Autowired
    public LeagueServiceImpl(LeagueRepository leagueRepository) {
        this.leagueRepository = leagueRepository;
    }

    @Override
    public League findByTitleOrCreate(String title) {
        League league = leagueRepository.findByTitle(title);
        if (league == null) {
            league = new League();
            league.setTitle(title);
            league = leagueRepository.save(league);
        }

        return league;
    }

    @Override
    @Transactional
    public League findById(Long leagueIdParam) {
        return leagueRepository.findOne(leagueIdParam);
    }
}
