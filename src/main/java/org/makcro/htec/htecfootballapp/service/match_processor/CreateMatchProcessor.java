package org.makcro.htec.htecfootballapp.service.match_processor;

import org.makcro.htec.htecfootballapp.data.FootballMatchRepository;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class CreateMatchProcessor implements MatchProcessor {

    private FootballMatchRepository footballMatchRepository;

    @Autowired
    public CreateMatchProcessor(@Value("${match.date.format}") String dateFormat, FootballMatchRepository footballMatchRepository) {
        this.footballMatchRepository = footballMatchRepository;
    }

    @Override
    public boolean process(MatchProcessorContext context) {
        FootballMatch match = createNewMatch(context);
        footballMatchRepository.save(match);
        return false;
    }

    protected FootballMatch createNewMatch(MatchProcessorContext context) {
        FootballMatch footballMatch = new FootballMatch();
        footballMatch.setAwayTeam(context.getAwayTeam());
        footballMatch.setGroup(context.getGroup());
        footballMatch.setHomeTeam(context.getHomeTeam());
        footballMatch.setKickoffAt(context.getKickoffAt());
        int matchDay = (context.getMatchResultResource().getMatchday());
        footballMatch.setMatchDay(matchDay);
        footballMatch.setScore(context.getMatchResultResource().getScore());

        return footballMatch;
    }
}
