package org.makcro.htec.htecfootballapp.service;

import org.makcro.htec.htecfootballapp.api.SearchParam;
import org.makcro.htec.htecfootballapp.api.resources.LeagueStandingsResource;
import org.makcro.htec.htecfootballapp.api.resources.MatchResultResource;
import org.makcro.htec.htecfootballapp.api.resources.GroupStandingsResource;

import java.util.List;

public interface FootballMatchService {
    GroupStandingsResource processGroupResults(List<MatchResultResource> matchResults);

    LeagueStandingsResource processLeagueResults(List<MatchResultResource> matchResults);

    GroupStandingsResource processSingleMatchResult(MatchResultResource matchResult);

    GroupStandingsResource updateMatch(Long matchId, MatchResultResource matchResultResource);

    List<MatchResultResource> search(SearchParam searchParam);
}
