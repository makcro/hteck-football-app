package org.makcro.htec.htecfootballapp;

import org.makcro.htec.htecfootballapp.service.match_processor.CreateMatchProcessor;
import org.makcro.htec.htecfootballapp.service.match_processor.EntityVerificationProcessor;
import org.makcro.htec.htecfootballapp.service.match_processor.MatchProcessor;
import org.makcro.htec.htecfootballapp.service.match_processor.UpdateMatchProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedList;
import java.util.List;

@Configuration
public class ContextConfiguration {

    @Autowired
    private EntityVerificationProcessor entityVerificationProcessor;

    @Autowired
    private CreateMatchProcessor createMatchProcessor;

    @Autowired
    private UpdateMatchProcessor updateMatchProcessor;

    @Bean(name = "processors")
    public List<MatchProcessor> getProcessors() {
        LinkedList<MatchProcessor> processors = new LinkedList<>();
        processors.add(entityVerificationProcessor);
        processors.add(updateMatchProcessor);
        processors.add(createMatchProcessor);

        return processors;
    }
}
