package org.makcro.htec.htecfootballapp.data;

import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "/teams")
public interface TeamRepository extends CrudRepository<Team, Long> {
    Team findByTitle(String title);
}