package org.makcro.htec.htecfootballapp.data;

import org.makcro.htec.htecfootballapp.api.ApiUrls;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "/groups")
public interface GroupRepository extends CrudRepository<Group, Long> {
    public Group findByTitle(String title);
    public Group findByTitleAndLeague(String title, League group);

}
