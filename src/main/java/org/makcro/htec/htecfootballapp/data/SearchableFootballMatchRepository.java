package org.makcro.htec.htecfootballapp.data;

import org.makcro.htec.htecfootballapp.api.SearchParam;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;

import java.util.List;

public interface SearchableFootballMatchRepository {

    List<FootballMatch> search(SearchParam searchParam);
}
