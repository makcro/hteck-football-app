package org.makcro.htec.htecfootballapp.data;

import com.sun.org.apache.bcel.internal.generic.GOTO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.makcro.htec.htecfootballapp.api.SearchParam;
import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.LinkedList;
import java.util.List;

@Component
public class SearchableFootballMatchRepositoryImpl implements SearchableFootballMatchRepository {


    @Autowired
    private EntityManager entityManager;

    @Autowired
    public SearchableFootballMatchRepositoryImpl(EntityManagerFactory managerFactory) {
    }

    @Override
    public List<FootballMatch> search(SearchParam searchParam) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<FootballMatch> query = criteriaBuilder.createQuery(FootballMatch.class);
        Root<FootballMatch> footballMatchRoot = query.from(FootballMatch.class);
        List<Predicate> criterions = new LinkedList<>();

        if (searchParam.getGroupName() != null) {
            Join<FootballMatch, Group> group = footballMatchRoot.join("group", JoinType.INNER);
            Predicate groupTitle = criteriaBuilder.equal(group.get("title"), searchParam.getGroupName());
            criterions.add(groupTitle);
        } else if (searchParam.getLeague() != null) {
            Join<FootballMatch, Group> group = footballMatchRoot.join("group", JoinType.INNER);
            Predicate league = criteriaBuilder.equal(group.get("league"), searchParam.getLeague());
            criterions.add(league);
        }

        if (searchParam.getGroup() != null) {
            Predicate group = criteriaBuilder.equal(footballMatchRoot.get("group"), searchParam.getGroup());
            criterions.add(group);
        }


        if (searchParam.getMatchDay() != null) {
            Predicate matchDay = criteriaBuilder.equal(footballMatchRoot.get("matchDay"), searchParam.getMatchDay());
            criterions.add(matchDay);
        }

        if (searchParam.getTeam() != null) {
            Join<FootballMatch, Team> homeTeamJoin = footballMatchRoot.join("homeTeam", JoinType.INNER);
            Join<FootballMatch, Team> awayTeamJoin = footballMatchRoot.join("awayTeam", JoinType.INNER);
            Predicate team = criteriaBuilder.or(criteriaBuilder.equal(homeTeamJoin.get("title"), searchParam.getTeam()),
                    criteriaBuilder.equal(awayTeamJoin.get("title"), searchParam.getTeam()));
            criterions.add(team);
        }

        if (searchParam.getKickoff() != null) {
            Predicate kickoffAt = criteriaBuilder.equal(footballMatchRoot.get("kickoffAt"), searchParam.getKickoff());
            criterions.add(kickoffAt);
        }

        if (!criterions.isEmpty()) {
            Predicate[] predicates = new Predicate[criterions.size()];
            criterions.toArray(predicates);
            query.where(predicates);
        }
        return entityManager.createQuery(query).getResultList();
    }
}
