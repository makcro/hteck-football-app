package org.makcro.htec.htecfootballapp.data;

import org.makcro.htec.htecfootballapp.data.entity.FootballMatch;
import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.League;
import org.makcro.htec.htecfootballapp.data.entity.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;
@RepositoryRestResource(path = "/matches")
public interface FootballMatchRepository extends CrudRepository<FootballMatch, Long> {
    List<FootballMatch> findByGroup(Group group);

    @Query("SELECT m FROM FootballMatch m where m.group.league = :league")
    List<FootballMatch> findByLeague(@Param("league") League league);

    FootballMatch findByHomeTeamAndAwayTeamAndKickoffAt(Team homeTeam, Team awayTeam, Date kickoffAt);
}
