package org.makcro.htec.htecfootballapp.data.entity;

import javax.persistence.*;

/**
 * Created by marko.lukic on 12/10/2017.
 */
@Entity
@Table(name = "LeagueGroup")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @ManyToOne(optional = false)
    @JoinColumn(name = "LEAGUE_ID", referencedColumnName = "ID")
    private League league;

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
