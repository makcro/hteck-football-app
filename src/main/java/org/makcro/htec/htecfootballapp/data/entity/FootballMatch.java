package org.makcro.htec.htecfootballapp.data.entity;

import org.makcro.htec.htecfootballapp.data.entity.Group;
import org.makcro.htec.htecfootballapp.data.entity.Team;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by marko.lukic on 12/7/2017.
 */
@Entity
public class FootballMatch implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID")
    private Group group;

    @Column
    private int matchDay;

    @ManyToOne
    @JoinColumn(name = "HOME_TEAM", referencedColumnName = "ID")
    private Team homeTeam;

    @ManyToOne
    @JoinColumn(name = "AWAY_TEAM", referencedColumnName = "ID")
    private Team awayTeam;

    @Column
    private Date kickoffAt;

    @Column
    private String score;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public void setMatchDay(int matchDay) {
        this.matchDay = matchDay;
    }

    public Date getKickoffAt() {
        return kickoffAt;
    }

    public void setKickoffAt(Date kickoffAt) {
        this.kickoffAt = kickoffAt;
    }

    public String getScore() {
        return score;
    }

    public int getMatchDay() {
        return matchDay;
    }

    public void setScore(String score) {
        this.score = score;
    }
}