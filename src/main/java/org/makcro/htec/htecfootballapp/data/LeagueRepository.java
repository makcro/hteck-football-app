package org.makcro.htec.htecfootballapp.data;

import org.makcro.htec.htecfootballapp.data.entity.League;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "/leagues")
public interface LeagueRepository extends CrudRepository<League, Long> {
    League findByTitle(String title);
}
